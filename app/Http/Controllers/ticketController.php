<?php

namespace App\Http\Controllers;

use \Exception;
use App\Client;
use Illuminate\Http\Request;

class ticketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get client data by id.
     *
     * @return JsonResponse
     */
    public function setTicket(Request $request)
    {

        try {
            $codigo = "200";
            $scID = $request->requerimiento["scotiaId"];

            $mensaje = $request->requerimiento["mensaje"];

            $canal = $request->requerimiento["canal"];

        //$users = User::where('movigoo_id',$request->movigoo_id)->get();

        //dd($request->requerimiento["canal"]);

        if ($scID== "") {
          $codigo = '400';
          throw new Exception ('No hay scotID.');
         
        }

        if ($mensaje== "") {
            $codigo = '404';
            throw new Exception ('No hay mensaje.');
        }

        if ($canal== "") {
            $codigo = '401';
            throw new Exception ('No hay canal.');
        }
        $status = 'success';
        $message = 'OK';
        //$result = $users;
      }
      catch(Exception $ex) {
        $status = 'exception';
        $codigo = '500';
        $message = $ex->getMessage();
      }

      $response = response()->json(
      ['respuesta'=>[
          'codigo'  => $codigo,
          'mensaje' => $message
      ]], 201);

      return $response;
    }
}